materia-kde (20220510-1) unstable; urgency=medium

  * New upstream release. 

 -- Boyuan Yang <byang@debian.org>  Sat, 14 May 2022 22:36:59 -0400

materia-kde (20220207-1) unstable; urgency=medium

  * New upstream release. 

 -- Boyuan Yang <byang@debian.org>  Mon, 14 Feb 2022 11:28:09 -0500

materia-kde (20211226-1) unstable; urgency=medium

  * New upstream release.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Wed, 29 Dec 2021 20:22:56 -0300

materia-kde (20211123-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Fri, 26 Nov 2021 08:58:09 -0500

materia-kde (20211027-1) unstable; urgency=medium

  [ Leandro Cunha ]
  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 4.6.0.

  [ Boyuan Yang ]
  * debian/control: Update package description using latest upstream
    information.

 -- Boyuan Yang <byang@debian.org>  Thu, 28 Oct 2021 18:10:41 -0400

materia-kde (20210814-1) unstable; urgency=medium

  * New upstream release.
  * debian/salsa-ci.yml: Minor update.
  * debian/gbp.conf: Revert to minimal format. User-specific options
    should be specified on the user level, not package level.

 -- Boyuan Yang <byang@debian.org>  Sat, 28 Aug 2021 16:51:05 -0400

materia-kde (20210129-1) unstable; urgency=medium

  * New upstream release.
  * Added materia-kde.lintian-overrides to archive of configuration
    theme.conf in folders in sddm/themes/*.
  * Added debian/salsa-ci.yml.
  * Changed debian/gbp.conf.
  * debian/control:
    - Added materia-gtk-theme in dependencies suggests field.
    - Updated description field with latest changes.
  * debian/copyright:
    - Updated year of author.
    - Updated year of myself contribution.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sun, 31 Jan 2021 22:54:45 -0300

materia-kde (20201222-1) unstable; urgency=medium

  * New upstream release

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sun, 27 Dec 2020 01:53:31 -0300

materia-kde (20201214-1) unstable; urgency=medium

  * New upstream release.

 -- Boyuan Yang <byang@debian.org>  Thu, 17 Dec 2020 14:52:00 -0500

materia-kde (20201113-1) unstable; urgency=medium

  [ Leandro Cunha ]
  * New upstream release.
  * Remove field Name from d/u/metadata reported by Janitor.

  [ Boyuan Yang ]
  * Bump Standards-Version to 4.5.1.

 -- Boyuan Yang <byang@debian.org>  Sun, 29 Nov 2020 11:45:00 -0500

materia-kde (20200907-1) unstable; urgency=medium

  * New upstream release.
  * Fixed Lintian reports.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Fri, 11 Sep 2020 03:07:27 -0300

materia-kde (20200812-1) unstable; urgency=medium

  * New upstream release

 -- Boyuan Yang <byang@debian.org>  Thu, 13 Aug 2020 23:45:03 -0400

materia-kde (20200713-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Add Debian Desktop Theme Team into uploaders
    list.

 -- Boyuan Yang <byang@debian.org>  Thu, 16 Jul 2020 23:51:52 -0400

materia-kde (20200614-2) unstable; urgency=medium

  * Source-only upload for testing migration.

 -- Boyuan Yang <byang@debian.org>  Thu, 16 Jul 2020 09:24:01 -0400

materia-kde (20200614-1) unstable; urgency=medium

  * Initial release (Closes: #944829).

 -- Leandro Cunha <leandrocunha016@gmail.com>  Tue, 30 Jun 2020 23:48:13 -0300
